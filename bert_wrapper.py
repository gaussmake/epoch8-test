import numpy as np
import torch
import pandas as pd
from tqdm import tqdm
from torch.utils.data import TensorDataset, SequentialSampler, DataLoader


def get_labels_ids_mapping(labels):
    return {label: i for i, label in enumerate(labels)}


def arrays_to_tensors(arrays, type):
    return [torch.tensor(array).type(type).cuda()
            for array in arrays]


def get_batch_count(sample_count, batch_size):
    return int(np.ceil(sample_count / float(batch_size)))


SENTIMENT_LABELS = ['negative', 'neutral', 'positive']
SENTIMENT_LABELS_MAP = get_labels_ids_mapping(SENTIMENT_LABELS)
assert 'negative' in SENTIMENT_LABELS_MAP
NEGATIVE_REASON_LABELS = ['Bad Flight',
                          'Can\'t Tell',
                          'Cancelled Flight',
                          'Customer Service Issue',
                          'Damaged Luggage',
                          'Flight Attendant Complaints',
                          'Flight Booking Problems',
                          'Late Flight',
                          'Lost Luggage',
                          'longlines']
NEGATIVE_REASON_LABELS_MAP = get_labels_ids_mapping(NEGATIVE_REASON_LABELS)
DEVICE = torch.device('cuda')


# region Model
class BertAirlineSentimentClassifierNetwork(torch.nn.Module):
    """
    BERT-based model for airline tweets sentiment classification.
    """

    def __init__(self, bert_encoder):
        """
        Initialize classifier
        :param bert_encoder: instance of BertModel
        """
        super(BertAirlineSentimentClassifierNetwork, self).__init__()
        self.bert_encoder = bert_encoder
        self.dropout = torch.nn.Dropout(bert_encoder.config.hidden_dropout_prob)
        self.sentiment_fc = torch.nn.Linear(bert_encoder.config.hidden_size, len(SENTIMENT_LABELS))
        self.negative_reason_fc = torch.nn.Linear(bert_encoder.config.hidden_size, len(NEGATIVE_REASON_LABELS))
        self.sentiment_softmax = torch.nn.Softmax(1)
        self.negative_reason_softmax = torch.nn.Softmax(1)

    def forward(self, input_ids, token_type_ids=None, attention_mask=None):
        _, pooled_embedding = self.bert_encoder(input_ids, token_type_ids, attention_mask,
                                                output_all_encoded_layers=False)
        dropped_embedding = self.dropout(pooled_embedding)
        sentiment_logits = self.sentiment_fc(dropped_embedding)
        negative_reason_logits = self.negative_reason_fc(dropped_embedding)
        sentiment = self.sentiment_softmax(sentiment_logits)
        negative_reason = self.negative_reason_softmax(negative_reason_logits)
        return sentiment, negative_reason

    def freeze_bert_encoder(self):
        for param in self.bert_encoder.parameters():
            param.requires_grad = False

    def unfreeze_bert_encoder(self):
        for param in self.bert_encoder.parameters():
            param.requires_grad = True
#endregion


#region Feature extraction
def text_to_features(text, tokenizer, sequence_length):
    """
    Convert text to BERT features with pre-trained tokenizer
    :param text: text
    :param tokenizer: tokenizer
    :param sequence_length: sequence length
    :return: 3 sequences (token ids, input mask, segment ids)
    """
    tokens = tokenizer.tokenize(text)
    if len(tokens) > sequence_length - 2:
        tokens = tokens[:sequence_length - 2]
    tokens = ['[CLS]'] + tokens + ['[SEP]']

    input_ids = tokenizer.convert_tokens_to_ids(tokens)
    input_mask = [1] * len(input_ids)
    segment_ids = [0] * len(input_ids)

    padding = [0] * (sequence_length - len(input_ids))
    input_ids += padding
    input_mask += padding
    segment_ids += padding

    return np.array(input_ids), np.array(input_mask), np.array(segment_ids)


def texts_to_features(texts, tokenizer, sequence_length):
    """
    Convert sequence of texts to BERT features with pre-trained tokenizer
    :param texts: texts
    :param tokenizer: tokenizer
    :param sequence_length: sequence length
    :return: 3 matrices(token ids, input mask, segment ids)
    """
    input_ids = []
    input_masks = []
    segment_ids = []
    for text in texts:
        text_input_ids, text_input_masks, text_segment_ids = text_to_features(text, tokenizer, sequence_length)
        input_ids.append(text_input_ids)
        input_masks.append(text_input_masks)
        segment_ids.append(text_segment_ids)

    input_ids = np.array(input_ids)
    input_masks = np.array(input_masks)
    segment_ids = np.array(segment_ids)

    return input_ids, input_masks, segment_ids
#endregion


#region Evaluation
def build_evaluate_dataset(texts, tokenizer, sequence_length):
    """
    Build dataset for model's evaluation
    :param texts: sequence of texts
    :param tokenizer: tokenizer
    :param sequence_length: sequence length
    :return: built TensorDataset with token_ids/input_masks/segment_ids
    """
    input_ids, input_masks, segment_ids = texts_to_features(texts, tokenizer, sequence_length)
    input_ids, input_masks, segment_ids = arrays_to_tensors([input_ids, input_masks, segment_ids],
                                                            torch.LongTensor)
    dataset = TensorDataset(input_ids, input_masks, segment_ids)
    return dataset


def evaluate(dataset, model, batch_size):
    """
    Evaluate model
    :param dataset: dataset from  ```build_evaluate_dataset```
    :param model: model
    :param batch_size: batch size
    :return: sentiments (N x 3 array), negative reasons (N x 10 array), each value in 0..1 range
    """
    sample_count = dataset.tensors[0].shape[0]
    sampler = SequentialSampler(dataset)
    loader = DataLoader(dataset, sampler=sampler, batch_size=batch_size)
    batches_sentiments = []
    batches_negative_reasons = []
    for input_ids, input_masks, segment_ids in tqdm(loader,
                                                    total=get_batch_count(sample_count, batch_size)):
        batch_sentiment, batch_negative_reasons = model(input_ids, input_masks, segment_ids)
        batches_sentiments.append(batch_sentiment.detach().cpu().numpy())
        batches_negative_reasons.append(batch_negative_reasons.detach().cpu().numpy())
    sentiments = np.vstack(batches_sentiments)
    negative_reasons = np.vstack(batches_negative_reasons)
    return sentiments, negative_reasons
#endregion


#region Training
class Loss(torch.nn.Module):
    def __init__(self, sentiment_class_weights, negative_reason_class_weights):
        super(Loss, self).__init__()
        sentiment_class_weights_tensor, negative_reason_class_weights_tensor = arrays_to_tensors([sentiment_class_weights,
                                                                                                  negative_reason_class_weights],
                                                                                                 torch.FloatTensor)
        self.sentiment_loss = torch.nn.NLLLoss(sentiment_class_weights_tensor)
        self.negative_reason_loss = torch.nn.NLLLoss(negative_reason_class_weights_tensor)

    def forward(self, sentiment_predicted, sentiment_true, negative_reason_predicted, negative_reason_true):
        sentiment_predicted_log = torch.log(sentiment_predicted + 1e-6)
        sentiment_loss = self.sentiment_loss(sentiment_predicted_log, sentiment_true) / len(SENTIMENT_LABELS)
        mask = sentiment_true == SENTIMENT_LABELS_MAP['negative']
        if mask.sum() > 0:
            negative_reason_predicted_log = torch.log(negative_reason_predicted + 1e-6)
            negative_reason_loss = self.negative_reason_loss(negative_reason_predicted_log[mask, :],
                                                             negative_reason_true[mask]) / len(NEGATIVE_REASON_LABELS)
        else:
            negative_reason_loss = 0.0
        return sentiment_loss + negative_reason_loss


def build_fit_dataset(texts, sentiment_labels, negative_reason_labels, tokenizer, sequence_length):
    input_ids, input_masks, segment_ids = texts_to_features(texts, tokenizer, sequence_length)
    input_ids, input_masks, segment_ids = arrays_to_tensors([input_ids, input_masks, segment_ids], torch.LongTensor)
    sentiment_target = pd.Series(sentiment_labels).apply(SENTIMENT_LABELS_MAP.get)
    negative_reason_target = pd.Series(negative_reason_labels).apply(lambda reason: NEGATIVE_REASON_LABELS_MAP.get(reason, -1))
    sentiment_target, negative_reason_target = arrays_to_tensors([sentiment_target.values,
                                                                  negative_reason_target.values],
                                                                 torch.LongTensor)
    return TensorDataset(input_ids, input_masks, segment_ids, sentiment_target, negative_reason_target)


def warmup_linear(x, warmup=0.002):
    if x < warmup:
        return x/warmup
    return 1.0 - x


def fit_epoch(dataset, model, optimizer, batch_size,
              previous_epoch_count,
              sentiment_class_weights, negative_reason_class_weights,
              t_total, gradient_accumulation_steps, base_learning_rate, warmup_proportion):
    loader = torch.utils.data.DataLoader(dataset, shuffle=True, batch_size=batch_size)
    batch_count = get_batch_count(dataset.tensors[0].shape[0], batch_size)

    model.train()
    loss = Loss(sentiment_class_weights, negative_reason_class_weights)
    for i, data in enumerate(tqdm(loader, total=batch_count)):
        input_ids, input_masks, segment_ids, sentiment_targets, negative_reason_targets = data

        optimizer.zero_grad()
        sentiment, negative_reason = model(input_ids, input_masks, segment_ids)
        loss_value = loss(sentiment, sentiment_targets, negative_reason, negative_reason_targets)
        loss_value.backward()

        if (i + 1) % gradient_accumulation_steps == 0:
            global_step = previous_epoch_count * batch_count + i
            lr = base_learning_rate * warmup_linear(global_step / t_total, warmup_proportion)
            for param_group in optimizer.param_groups:
                param_group['lr'] = lr

        optimizer.step()
#endregion